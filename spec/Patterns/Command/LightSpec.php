<?php

namespace spec\Patterns\Command;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class LightSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType('Patterns\Command\Light');
    }

    public function it_should_start_off()
    {
        $this->getStatus()
            ->shouldBe(false);
    }

    public function it_should_change_light_status()
    {
        $this->getStatus()->shouldBe(false);
        $this->execute()->shouldBe(true);
        $this->getStatus()->shouldBe(true);
    }
}
