<?php

namespace spec\Patterns\Command;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class AirSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType('Patterns\Command\Air');
    }

    public function it_starts_off()
    {
        $this->getStatus()
            ->shouldBe(false);
    }

    public function it_changes_air_status()
    {
        $this->getStatus()->shouldBe(false);
        $this->execute()->shouldBe(true);
        $this->getStatus()->shouldBe(true);
    }
}
