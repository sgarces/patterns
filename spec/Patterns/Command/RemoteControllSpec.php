<?php

namespace spec\Patterns\Command;

use Patterns\Command\ICommand;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Patterns\Command\Light;

class RemoteControllSpec extends ObjectBehavior
{

    public function it_is_initializable()
    {
        $this->shouldHaveType('Patterns\Command\RemoteControll');
    }

    private function addMockedCommand()
    {
        $command = new Light();
        $this->addCommand($command);
    }

    private function addCommands($commands_number)
    {
        if (is_int($commands_number)){

            for ($i = 0; $i < $commands_number; $i++){
                $this->addMockedCommand();
            }

        }else{
            throw new \InvalidArgumentException('addCommands needs integer, '.gettype($commands_number).' given');
        }
    }

    public function it_adds_commands(ICommand $command1, ICommand $command2)
    {
        $this->addCommand($command1)->shouldHaveCount(1);
        $this->addCommand($command2)->shouldHaveCount(2);
    }

    public function it_gets_slots_status()
    {
        $this->addCommands(3);
        $this->getSlots()->shouldHaveCount(3);
    }

    public function it_executes_commands()
    {
        $this->addCommands(5);
        $slots = $this->getSlots();
        $slots->shouldHaveCount(5);
        for($i = 0; $i < count($slots); $i++){
            $slots[$i]->getStatus()->shouldBe(false);
        }
        $this->execute()->shouldBe(true);
        $slots = $this->getSlots();
        for($i = 0; $i < count($slots); $i++){
            $slots[$i]->getStatus()->shouldBe(true);
        }
    }

    public function it_catches_execution_exceptions(ICommand $command1)
    {
        $command1->execute()->willThrow(new \Exception());
        $this->addCommand($command1);
        $this->execute()->shouldBe(false);
    }
}
