<?php

namespace spec\Patterns\Decorator\Condiments;

use Patterns\Decorator\Beverages\Expresso;
use Patterns\Decorator\Condiments\Milk;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class MochaSpec extends ObjectBehavior
{
    public function getMatchers()
    {
        return parent::getMatchers() + [
            'returnDecimal' => function($subject, $value, $precision = 14) {
                return abs($subject - $value) < pow(10, -$precision);
            },
        ];
    }

    function let(Expresso $beverage){
        $beverage->getDescription()->willReturn('Expresso');
        $beverage->getCost()->willReturn(1.99);
        $this->beConstructedWith($beverage);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Patterns\Decorator\Beverage');
        $this->shouldHaveType('Patterns\Decorator\Condiments\Mocha');
    }

    function it_returns_description()
    {
        $this->getDescription()->shouldBe('Expresso, Mocha');
    }

    function it_returns_description_for_more_condiments(Milk $milk)
    {
        $milk->getDescription()->willReturn('Expresso, Milk');
        $this->beverage = $milk;
        $this->getDescription()->shouldBe('Expresso, Milk, Mocha');
    }

    function it_returns_cost()
    {
        $this->getCost()->shouldReturnDecimal(2.24);
    }

    function it_returns_cost_for_more_condiments(Milk $milk)
    {
        $milk->getCost()->willReturn(2.09);
        $milk->getCost()->shouldBeCalled();
        $this->beverage = $milk;
        $this->getCost()->shouldReturn(2.34);
    }
}
