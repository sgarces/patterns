<?php

namespace spec\Patterns\Decorator\Condiments;

use Patterns\Decorator\Beverages\Expresso;
use Patterns\Decorator\Condiments\Mocha;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class MilkSpec extends ObjectBehavior
{
    public function getMatchers()
    {
        return parent::getMatchers() + [
            'returnDecimal' => function($subject, $value, $precision = 14) {
                return abs($subject - $value) < pow(10, -$precision);
            },
        ];
    }

    function let(Expresso $beverage){
        $beverage->getDescription()->willReturn('Expresso');
        $beverage->getCost()->willReturn(1.99);
        $this->beConstructedWith($beverage);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Patterns\Decorator\Beverage');
        $this->shouldHaveType('Patterns\Decorator\Condiments\Milk');
    }

    function it_returns_description()
    {
        $this->getDescription()->shouldBe('Expresso, Milk');
    }

    function it_returns_description_for_more_condiments(Mocha $mocha)
    {
        $mocha->getDescription()->willReturn('Expresso, Mocha');
        $this->beverage = $mocha;
        $this->getDescription()->shouldBe('Expresso, Mocha, Milk');
    }

    function it_returns_cost()
    {
        $this->getCost()->shouldReturnDecimal(2.09);
    }

    function it_returns_cost_for_more_condiments(Mocha $mocha)
    {
        $mocha->getCost()->willReturn(2.24);
        $mocha->getCost()->shouldBeCalled();
        $this->beverage = $mocha;
        $this->getCost()->shouldReturnDecimal(2.34);
    }
}
