<?php

namespace spec\Patterns\Decorator;

use Patterns\Decorator\Beverages\Expresso;
use Patterns\Decorator\Beverage;
use Patterns\Decorator\IBeverage;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class BeveragesFactorySpec extends ObjectBehavior
{

    function getCondiments($withNulls = false)
    {
        $return = [
            ['milk'],
            ['milk', 'mocha'],
            ['milk', 'mocha', 'milk'],
        ];
        if ($withNulls) {
            $return[] = [' '];
        }

        return $return;
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Patterns\Decorator\BeveragesFactory');
    }

    /**
     * @dataProvider getBeverages()
     */
    function it_builds_beverages(Beverage $beverage)
    {
        $condiments = $this->getCondiments(true);
        foreach ($condiments as $beverageCondiments) {
            $this->getBeverage($beverage, $beverageCondiments)->shouldReturnAnInstanceOf('Patterns\Decorator\IBeverage');
        }
    }
}
