<?php

namespace spec\Patterns\Decorator\Beverages;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ExpressoSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Patterns\Decorator\Beverage');
        $this->shouldHaveType('Patterns\Decorator\Beverages\Expresso');
    }

    function it_returns_cost()
    {
        $this->getCost()->shouldReturn(1.99);
    }

    function it_returns_description()
    {
        $this->getDescription()->shouldReturn('Expresso');
    }
}
