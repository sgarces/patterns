<?php

namespace spec\Patterns\Decorator\Beverages;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class HouseBlendSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Patterns\Decorator\Beverage');
        $this->shouldHaveType('Patterns\Decorator\Beverages\HouseBlend');
    }

    function it_returns_cost()
    {
        $this->getCost()->shouldReturn(0.89);
    }

    function it_returns_description()
    {
        $this->getDescription()->shouldReturn('House Blend');
    }
}
