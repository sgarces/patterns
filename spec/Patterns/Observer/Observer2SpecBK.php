<?php

namespace spec\Patterns\Observer;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class Observer2Spec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Patterns\Observer\Observer2');
    }
}
