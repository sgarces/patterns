<?php

namespace spec\Patterns\Observer\Error;

use Patterns\Observer\Contracts\IObserver;
use Patterns\Observer\Subject;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class UpdateObserverErrorSpec extends ObjectBehavior
{

    public function it_is_initializable()
    {
        $this->shouldHaveType('Patterns\Observer\Error\UpdateObserverError');
    }


    public function let(IObserver $ob1)
    {
        $sub = new Subject();
        $this->beConstructedWith($ob1, $sub);
    }
}
