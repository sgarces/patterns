<?php

namespace spec\Patterns\Observer;

use Patterns\Observer\Contracts\IObserver;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class SubjectSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType('Patterns\Observer\Subject');
    }

    public function it_registers_observers(IObserver $observer)
    {
        $this->countObservers()->shouldReturn(0);
        $this->registerObserver($observer);
        $this->countObservers()->shouldReturn(1);
    }

    public function it_finds_observer(IObserver $observer)
    {
        $this->registerObserver($observer);
        $this->getObserver($observer)->shouldReturn($observer);
    }

    public function it_removesObservers(IObserver $ob1, IObserver $ob2)
    {
        $this->countObservers()->shouldReturn(0);
        $this->registerObserver($ob1);
        $this->registerObserver($ob2);
        $this->countObservers()->shouldReturn(2);
        $this->removeObserver($ob1);
        $this->countObservers()->shouldReturn(1);
        $this->getObserver($ob2)->shouldReturn($ob2);
        $this->getObserver($ob1)->shouldReturn(false);
    }

    public function it_notifies_observers(IObserver $ob1, IObserver $ob2)
    {
        $ob1->update(null, null, null)->shouldBeCalled()->willReturn(true);
        $ob2->update(null, null, null)->shouldBeCalled()->willReturn(true);
        $this->registerObserver($ob1);
        $this->registerObserver($ob2);
        $this->notifyObservers()->shouldReturn(true);
    }


    public function it_notifies_observers_when_temp_changes(IObserver $ob1, IObserver $ob2)
    {
        $temp = 25;
        $ob1->update($temp, null, null)->shouldBeCalled();
        $ob2->update($temp, null, null)->shouldBeCalled();
        $this->registerObserver($ob1);
        $this->registerObserver($ob2);
        $this->setTemp($temp);
    }

    public function it_notifies_observers_when_hum_changes(IObserver $ob1, IObserver $ob2)
    {
        $hum = 25;
        $ob1->update(null, $hum, null)->shouldBeCalled();
        $ob2->update(null, $hum, null)->shouldBeCalled();
        $this->registerObserver($ob1);
        $this->registerObserver($ob2);
        $this->setHum($hum);
    }

    public function it_notifies_observers_when_press_changes(IObserver $ob1, IObserver $ob2)
    {
        $press = 25;
        $ob1->update(null, null, $press)->shouldBeCalled();
        $ob2->update(null, null, $press)->shouldBeCalled();
        $this->registerObserver($ob1);
        $this->registerObserver($ob2);
        $this->setPress($press);
    }


    public function it_catches_notification_errors(IObserver $ob1, IObserver $ob2)
    {
        $ob1->update(null, null, null)->willReturn(false);
        $ob2->update(null, null, null)->willReturn(false);
        $this->registerObserver($ob1);
        $this->registerObserver($ob2);
        $this->notifyObservers();
        $errors = $this->getObserversErrors();
        $errors->shouldBeArray();
        foreach ($errors as $error) {
            $error->shouldBeAnInstanceOf('Patterns\Observer\Error\UpdateObserverError');
        }
    }


    public function it_throws_exception_if_no_error_class_is_found_setting_observerErrors($ob1)
    {
        $errors = [$ob1];
        $this->shouldThrow('TypeError')->during('setObserversErrors', [$errors]);
        $this->shouldThrow('TypeError')->during('setObserversErrors', [$ob1]);
    }

    public function it_throws_exception_adding_no_error_objects()
    {
        $this->shouldThrow('TypeError')->during('addObserverError', ['string']);
    }

    public function it_sets_observersErrors(IObserver $ob1, IObserver $ob2)
    {
        $this->setObserversErrors([$ob1, $ob2])->shouldReturn($this);
    }

    public function it_return_observers(IObserver $ob1, IObserver $ob2)
    {
        $this->registerObserver($ob1);
        $this->registerObserver($ob2);
        $this->getObservers()->shouldBe([$ob1, $ob2]);
    }

    public function it_throws_exception_setting_non_observer_obj(IObserver $ob1, $ob2)
    {
        $this->shouldThrow('Exception')->during('setObservers', [[$ob1, $ob2]]);
    }

    public function it_can_set_an_array_of_observers(IObserver $ob1, IObserver $ob2)
    {
        $this->setObservers([$ob1, $ob2])->shouldReturn($this);
    }
}
