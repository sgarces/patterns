<?php

namespace Patterns\Observer\Error;

use Patterns\Observer\Contracts\IObserver;
use Patterns\Observer\Contracts\ISubject;

class UpdateObserverError
{
    public $observer;
    public $subjectStatus;


    public function __construct(IObserver $observer, ISubject $subject)
    {
        $this->observer = get_class($observer);
        $this->subjectStatus = $subject->getDataAsArray();
    }
}
