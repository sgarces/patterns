<?php

namespace Patterns\Observer;

use Patterns\Observer\Contracts\IObserver;
/**
 * @codeCoverageIgnore
 */
class Observer2 implements IObserver
{
    public function update($temp, $hum, $press)
    {
        echo PHP_EOL.'Temperatura: '.$temp;
        echo PHP_EOL.'Humedad: '.$hum;
        echo PHP_EOL.'Presion: '.$press;
        echo PHP_EOL.'----------'.PHP_EOL;
    }
}
