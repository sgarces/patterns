<?php
namespace Patterns\Observer;

class Weather
{
    private $subject;

    public function __construct()
    {
        $this->subject = new Subject();
        $ob1 = new Observer1();
        $ob2 = new Observer2();
        $this->subject->registerObserver($ob1);
        $this->subject->registerObserver($ob2);

        $this->subject->setHum(25);
        $this->subject->setTemp(20);
        $this->subject->setPress(15);

        $this->subject->setHum(30);
        $this->subject->setTemp(10);
        $this->subject->setPress(20);

        /** @noinspection PhpUndefinedMethodInspection */
        if ($this->subject->getErrors() !== []) {
            /** @noinspection PhpUndefinedMethodInspection */
            echo PHP_EOL . 'ERRORES: ' . print_r($this->subject->getErrors(), 1);
        }
    }
}
