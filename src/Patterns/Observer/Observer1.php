<?php

namespace Patterns\Observer;

use Patterns\Observer\Contracts\IObserver;
/**
 * @codeCoverageIgnore
 */
class Observer1 implements IObserver
{
    const FILE_NAME = './mesures.csv';
    const FILE_MODE = 'a+';

    public function update($temp, $hum, $press)
    {
        try {
            $file = fopen(self::FILE_NAME, self::FILE_NAME);
            fputcsv($file, [$temp, $hum, $press]);
            fclose($file);
        } catch (\Exception $ex) {
            error_log(
                'Unable to write file ' .
                self::FILE_NAME . '(' . self::FILE_MODE . '): ' . PHP_EOL .
                $ex->getMessage() . PHP_EOL .
                $ex->getTraceAsString()
            );
        }
    }
}
