<?php

namespace Patterns\Observer\Contracts;

interface ISubject
{
    public function registerObserver(IObserver $observer);
    public function removeObserver(IObserver $observer);
    public function notifyObservers();

    public function getDataAsArray();
}
