<?php
/**
 * Date: 1/11/15
 * Time: 20:26
 */

namespace Patterns\Observer\Contracts;


interface IObserver
{
    public function update($temp, $hum, $press);
}
