<?php

namespace Patterns\Observer;

use Patterns\Observer\Contracts\IObserver;
use Patterns\Observer\Contracts\ISubject;
use Patterns\Observer\Error\UpdateObserverError;
use InvalidArgumentException;
use Exception;

class Subject implements ISubject
{
    private $temp;
    private $hum;
    private $press;

    private $observers = [];

    private $observersErrors = [];

    /**
     * @return array
     */
    public function getObserversErrors()
    {
        return $this->observersErrors;
    }

    /**
     * @param array $observersErrors
     * @return Subject
     */
    public function setObserversErrors(Array $observersErrors)
    {
        foreach ($observersErrors as $error) {
            $this->addObserverError($error);
        }

        return $this;
    }

    public function addObserverError(IObserver $observer)
    {
        $error = new UpdateObserverError($observer, $this);
        $this->observersErrors[] = $error;
    }

    public function countObservers()
    {
        return count($this->observers);
    }

    /**
     * @return mixed
     */
    public function getTemp()
    {
        return $this->temp;
    }

    /**
     * @param mixed $temp
     * @return Subject
     */
    public function setTemp($temp)
    {
        $this->temp = $temp;
        $this->notifyObservers();

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHum()
    {
        return $this->hum;
    }

    /**
     * @param mixed $hum
     * @return Subject
     */
    public function setHum($hum)
    {
        $this->hum = $hum;
        $this->notifyObservers();

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPress()
    {
        return $this->press;
    }

    /**
     * @param mixed $press
     * @return Subject
     */
    public function setPress($press)
    {
        $this->press = $press;
        $this->notifyObservers();

        return $this;
    }

    /**
     * @return array
     */
    public function getObservers()
    {
        return $this->observers;
    }

    /**
     * @param array $observers
     * @return Subject
     */
    public function setObservers(Array $observers)
    {
        foreach ($observers as $observer) {
            if (!($observer instanceof IObserver)) {
                throw new InvalidArgumentException('Invalid data');
            }
        }
        $this->observers = $observers;

        return $this;
    }

    public function getObserver(IObserver $searchedObserver)
    {
        foreach ($this->observers as $observer) {
            if ($observer === $searchedObserver) {
                return $observer;
            }
        }

        return false;
    }

    public function registerObserver(IObserver $observer)
    {
        $this->observers[] = $observer;
    }

    public function removeObserver(IObserver $observer)
    {
        $index = array_search($observer, $this->observers, true);
        if (false !== $index) {
            unset($this->observers[$index]);
        }
    }

    public function notifyObservers()
    {
        foreach ($this->observers as $observer) {
            try {
                /** @noinspection PhpUndefinedMethodInspection */
                $result = $observer->update($this->getTemp(), $this->getHum(), $this->getPress());
                if (!$result) {
                    $this->addObserverError($observer);
                }
            } catch (Exception $ex) {
                $this->addObserverError($observer);
            }
        }

        return true;
    }

    public function getDataAsArray()
    {
        return [
            'hum'   => $this->getHum(),
            'temp'  => $this->getTemp(),
            'press' => $this->getPress()
        ];
    }
}
