<?php

namespace Patterns\Command;

class Light implements ICommand
{
    private $_light = false;

    public function execute()
    {
        $this->_light = !$this->_light;
        return $this->_light;
    }

    public function getStatus()
    {
        return $this->_light;
    }
}
