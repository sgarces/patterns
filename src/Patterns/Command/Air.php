<?php

namespace Patterns\Command;

class Air implements ICommand
{
    private $_air = false;

    public function getStatus()
    {
        return $this->_air;
    }

    public function execute()
    {
        $this->_air = !($this->_air);
        return $this->_air;
    }

}
