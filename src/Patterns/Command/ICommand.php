<?php

namespace Patterns\Command;

interface ICommand
{
    public function execute();
//    public function getStatus();
}
