<?php

namespace Patterns\Command;

class RemoteControll
{
    protected $slots = [];

    public function addCommand(ICommand $iCommand)
    {
        $this->slots[] = $iCommand;
        return $this->getSlots();
    }

    public function getSlots()
    {
        return $this->slots;
    }

    public function execute()
    {
        try {
            foreach ($this->slots as $slot) {
                /** @noinspection PhpUndefinedMethodInspection */
                $slot->execute();
            }
            return true;
        }catch (\Exception $ex){
            return false;
        }
    }
}
