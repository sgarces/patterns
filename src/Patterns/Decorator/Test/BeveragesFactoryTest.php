<?php
/**
 * Date: 8/11/15
 * Time: 0:31
 */

namespace Patterns\Decorator\Test;

use Patterns\Decorator\Beverages\Expresso;
use Patterns\Decorator\BeveragesFactory;
use PHPUnit_Framework_TestCase;

class BeveragesFactoryTest extends PHPUnit_Framework_TestCase
{

    public function testCreatesSimpleBeverages()
    {
        $base = new Expresso();
        $condiments = [];
        $express = BeveragesFactory::getBeverage($base, $condiments);
        $this->assertEquals($base, $express);
    }

    public function testCreatesBeveragesWithMilk()
    {
        $base = new Expresso();
        $condiments = ['milk'];
        $express = BeveragesFactory::getBeverage($base, $condiments);
        $this->assertInstanceOf('Patterns\\Decorator\\IBeverage', $express);
    }

    public function testCreatesBeveragesWithMocha()
    {
        $base = new Expresso();
        $condiments = ['mocha'];
        $express = BeveragesFactory::getBeverage($base, $condiments);
        $this->assertInstanceOf('Patterns\\Decorator\\IBeverage', $express);
    }

    public function testCreatesBeveragesWithCondiments()
    {
        $base = new Expresso();
        $condiments = ['mocha', 'milk'];
        $express = BeveragesFactory::getBeverage($base, $condiments);
        $this->assertInstanceOf('Patterns\\Decorator\\IBeverage', $express);
    }
}
