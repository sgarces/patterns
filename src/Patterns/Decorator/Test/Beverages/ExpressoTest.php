<?php

namespace Patterns\Decorator\Beverages\Test;

use Patterns\Decorator\Beverages\Expresso;
use PHPUnit_Framework_TestCase;

class ExpressoTest extends PHPUnit_Framework_TestCase
{
    public function testReturnCost()
    {
        $express = new Expresso();
        $this->assertEquals(1.99, $express->getCost());
    }

    public function testReturnDescription()
    {
        $express = new Expresso();
        $this->assertEquals('Expresso', $express->getDescription());
    }
}
