<?php
/**
 * Date: 7/11/15
 * Time: 13:47
 */

namespace Patterns\Decorator\Test\Beverages;

use Patterns\Decorator\Beverages\HouseBlend;
use PHPUnit_Framework_TestCase;

class HouseBlendTest extends PHPUnit_Framework_TestCase
{
    public function testReturnCost()
    {
        $express = new HouseBlend();
        $this->assertEquals($express->getCost(), 0.89);
    }

    public function testReturnDescription()
    {
        $express = new HouseBlend();
        $this->assertEquals('House Blend', $express->getDescription());
    }
}
