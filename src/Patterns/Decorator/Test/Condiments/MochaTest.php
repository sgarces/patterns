<?php
/**
 * Date: 7/11/15
 * Time: 22:55
 */

namespace Patterns\Decorator\Condiments;

use Patterns\Decorator\Beverages\HouseBlend;
use PHPUnit_Framework_TestCase;
use Patterns\Decorator\Beverages\Expresso;

class MochaTest extends PHPUnit_Framework_TestCase
{
    public function testReturnCost()
    {
        $mocha = new Mocha(new Expresso());
        $this->assertEquals(2.24, $mocha->getCost());
        $mocha = new Mocha(new Milk(new Expresso()));
        $this->assertEquals(2.34, $mocha->getCost());
        $mocha = new Mocha(new Mocha(new Milk(new Expresso())));
        $this->assertEquals(2.59, $mocha->getCost());
    }

    public function testReturnDescription()
    {
        $mocha = new Mocha(new Expresso());
        $this->assertEquals('Expresso, Mocha', $mocha->getDescription());
        $mocha = new Mocha(new HouseBlend());
        $this->assertEquals('House Blend, Mocha', $mocha->getDescription());
        $mocha = new Mocha(new Mocha(new HouseBlend()));
        $this->assertEquals('House Blend, Mocha, Mocha', $mocha->getDescription());
    }
}
