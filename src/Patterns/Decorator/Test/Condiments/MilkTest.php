<?php
/**
 * Date: 7/11/15
 * Time: 22:52
 */

namespace Patterns\Decorator\Condiments;

use Patterns\Decorator\Beverages\HouseBlend;
use PHPUnit_Framework_TestCase;
use Patterns\Decorator\Beverages\Expresso;

class MilkTest extends PHPUnit_Framework_TestCase
{
    public function testReturnCost()
    {
        $milk = new Milk(new Expresso());
        $this->assertEquals(2.09, $milk->getCost());
        $milk = new Milk(new Mocha(new Expresso()));
        $this->assertEquals(2.34, $milk->getCost());
        $milk = new Milk(new Milk(new Mocha(new Expresso())));
        $this->assertEquals(2.44, $milk->getCost());
    }

    public function testReturnDescription()
    {
        $milk = new Milk(new Expresso());
        $this->assertEquals('Expresso, Milk', $milk->getDescription());
        $milk = new Milk(new HouseBlend());
        $this->assertEquals('House Blend, Milk', $milk->getDescription());
        $milk = new Milk(new Milk(new HouseBlend()));
        $this->assertEquals('House Blend, Milk, Milk', $milk->getDescription());
    }
}
