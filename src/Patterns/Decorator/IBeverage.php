<?php

namespace Patterns\Decorator;

interface IBeverage
{
    /**
     * Returns the beverage cost
     * @return Float
     */
    public function getCost();

    /**
     * Obtains the beverage description
     * @return Int
     */
    public function getDescription();
}
