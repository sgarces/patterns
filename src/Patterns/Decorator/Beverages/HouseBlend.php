<?php

namespace Patterns\Decorator\Beverages;

use Patterns\Decorator\Beverage;

class HouseBlend extends Beverage
{
    private $cost = 0.89;
    private $description = 'House Blend';

    public function getDescription()
    {
        return $this->description;
    }

    public function getCost()
    {
        return $this->cost;
    }
}
