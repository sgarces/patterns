<?php

namespace Patterns\Decorator\Beverages;

use Patterns\Decorator\Beverage;

class Expresso extends Beverage
{
    private $cost = 1.99;
    private $description = 'Expresso';

    public function getDescription()
    {
        return $this->description;
    }

    public function getCost()
    {
        return $this->cost;
    }
}
