<?php

namespace Patterns\Decorator;

abstract class Beverage implements IBeverage
{

    /**
     * Returns the beverage cost
     * @return Float
     */
    abstract public function getCost();

    /**
     * Obtains the beverage description
     * @return Int
     */
    abstract public function getDescription();
}
