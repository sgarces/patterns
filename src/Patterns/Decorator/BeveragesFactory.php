<?php

namespace Patterns\Decorator;

class BeveragesFactory
{
    public static function getBeverage(Beverage $baseBeverage, $condiments)
    {
        $currentBeverage = $baseBeverage;
        foreach ($condiments as $condiment) {
            $condimentName = __NAMESPACE__ . '\Condiments\\' . ucfirst($condiment);
            if (class_exists($condimentName)) {
                $currentBeverage = new $condimentName($currentBeverage);
            }
        }

        return $currentBeverage;
    }
}
