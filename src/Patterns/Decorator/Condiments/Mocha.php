<?php

namespace Patterns\Decorator\Condiments;

use Patterns\Decorator\Beverage;
use Patterns\Decorator\CondimentDecorator;

class Mocha extends CondimentDecorator
{
    public $beverage;
    private $cost = 0.25;
    private $description = 'Mocha';

    public function __construct(Beverage $beverage)
    {
        $this->beverage = $beverage;
    }

    public function getDescription()
    {
        return $this->beverage->getDescription() . ', '.$this->description;
    }

    public function getCost()
    {
        return $this->beverage->getCost() + $this->cost;
    }
}
